# CCSEP Further Assessment
= Individual Assessment
= Topic: Log Injection

Author: Jayden Tan

IMPORTANT NOTE: Regex replacement was unable to be fixed in time along with Make not able to be installed properly on my machine (Did not test out Makefile - Generated through documentation and prior program Makefiles)

= About: 
This program highlights how a malicious user could write unvalidated content into the logging system of an application.

The master branch contains the vulnerable application where user input allows malicious content to be written to the log file.

The patch branch contains the patched application where user input is validated to be a positive integer in this scenario and replaces all problematic characters if needed to disallow log injection.

= How to run
The program assumes that JDK 11.0.8 or higher is installed along with Make.

To run the main program enter:

    make run

To run the app functionality test enter:
    
    make test

To run the vulnerability patch test enter:

    make test-patch

== Detection, Explotation and Patching Vulnerability
To detect the vulnerability, the patched program goes through 2 security checks. Initial check is to validate the user input (Positive integer for a transfer of X amount) as to not allow any other type of datatype. The 2nd check detects for certain problematic characters that can be used to log inject (\n, \r, 0x8)/

To exploit the vulnerability, the user just needs to trigger the amount as invalid ("Twenty-five") and then add to the end of their input to forge a log entry into the log file. This would result in a log injection of malicious content to be exploited depending on the form (Incorrect logging of exception).

To patch the vulnerability, both security checks are used. Basic user input validation through java datatypes and built-in regex replaceAll function reduces the chances of a log injection line and ensures only legitimate exceptions are logged accordingly.

== License

See link:LICENSE[]


