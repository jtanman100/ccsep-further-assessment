//Test Package
package test;

//Imports
import src.controller.*;
/*
    Test Harness for program
    Created for CCSEP Further Assessment 
    Date modified: 15 Feb 2021

    @author Jayden Tan
*/
public class UnitTest
{
    static int tests, passed;
	static boolean debug;
    public static void main (String[] args)
    {
        init(args);
        Main app = new Main();

        test("Positive Amount as a String");
        try {
            String input = "30";
            System.out.println("Test positive amount is " + input);
            boolean valid = app.userInput(input);
            if (valid == true)
            {
                pass();
            }
        } catch (Exception e2) {
            fail(e2.getMessage());
        }

        test("Positive Amount as a written String");
        try {
            String input = "Fourty";
            System.out.println("Test positive amount is " + input);
            boolean valid = app.userInput(input);
			if (valid == true)
            {
                pass();
            }
        } catch (Exception e2) {
            fail(e2.getMessage());
        }

		summary();
    }

    //---- GENERIC TEST HARNESS FRAMEWORK ------- (Adjusted from ISEC3004 Group Assignment 2020 Semester 2)

	public static void init(String[] args) {
		tests = 0;
		passed = 0;
		
		if (args.length > 0) {
			if (args[0].equals("d")) {
				debug = true;
				System.out.println("Running in debug mode...");
			} else {
				System.out.println("Invalid mode argument - ignoring...");
				System.out.println("Note: Running with 'd' runs the program in debug mode.");
				debug = false;
			}
		} else {
			debug = false;
		}
	}
	
	public static void test(String title) {
		tests++;
		System.out.print("\nTest "+tests+" - "+title+": ");
	}
	
	public static void pass() {
		System.out.println("PASSED");
		passed++;
	}
	
	public static void passIf(boolean cond) {
		if (cond) 
		{
			System.out.println("PASSED");
			passed++;
		} else {
			fail();
		}
	}
	
	public static void passIf(boolean cond, String failMsg) {
		if (cond) 
		{
			System.out.println("PASSED");
			passed++;
		} else {
			fail(failMsg);
		}
	}
	
	public static void fail() {
		System.out.println("FAILED");
	}
	
	public static void fail(String message) {
		System.out.println("FAILED - "+message);
	}
	
	public static void summary() {
		System.out.println("\nSummary:\n"+passed+"/"+tests+" Passed - "+(((double)passed/(double)tests)*100.0)+"%");
	}

}