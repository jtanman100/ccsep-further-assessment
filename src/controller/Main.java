package src.controller;

import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Main {
    
    static Logger logger;
    public static void main(String[] args) throws Exception
    {
        boolean append = true;
        boolean validAmount = false;

        /**Writing log entries to a log file*/
        
        //New FileHandler to write to specific file for logging
        FileHandler handler = new FileHandler("default.Log", append);

        //New Logger instance 
        logger = Logger.getLogger("src.controller");
        logger.addHandler(handler);

        Main main = new Main();

        Scanner scanner = new Scanner(System.in);/**Scanner initialised for User input*/
        System.out.println("Enter amount to transfer");
        String amount = scanner.next();
        validAmount = main.userInput(amount);/**Converts user input string into a integer else exception*/
        if (validAmount == true)
        {
            System.out.println("Amount entered is " + amount);
        }
    }

    /**Method to convert user input string into a integer if possible
     * Catch: NumberFormatException
     * Result: Logs resultant exception into default logfile
     */
    public boolean userInput (String amount)
    {
        boolean valid = false;
        try {
            int convertedAmount = Integer.parseInt(amount);
            valid = true;
        }
        catch (NumberFormatException e) {
            logger.info("Unable to parse amount = " + amount);
        }
        return valid;
    }
}
