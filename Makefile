SDK_PATH = javafx-sdk-11.0.2/lib
SRC = src/controller/*.java test/*.java

build:
	javac --module-path $(SDK_PATH) --add-modules $(SRC)

run: build
	java --module-path $(SDK_PATH) --add-modules src/controller/Main

test: build
	java test/UnitTest

test-patch: build
	java test/UnitSecurityTest

clean:
	rm src/controller/*.class test/*.class
 